SUMMARY = "meta-tri-outdoor kernel config fragment recipe"
DESCRIPTION = "meta-tri-outdoor kernel config fragment recipe"
LICENSE = "MIT"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
SRC_URI += "file://tri.cfg"

do_configure_append() {
    cat ${WORKDIR}/*.cfg >> ${B}/.config
}

