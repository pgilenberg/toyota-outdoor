SUMMARY = "meta-tri-outdoor kernel config fragment recipe"
DESCRIPTION = "meta-tri-outdoor kernel config fragment recipe"
LICENSE = "MIT"

FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
SRC_URI += "file://u-boot.patch "
